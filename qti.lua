

function qtiscore (opt)
   -- check mandatory opt
   if type(opt.points) ~= "number" then
      error("no points given")
   elseif type(opt.result) ~= "number" then
      error("no result")
   end

   -- everything else is optional
   if type(opt.percentband) == "number" then
      minval = (100.-opt.percentband)/100. * opt.result
      maxval = (100.+opt.percentband)/100. * opt.result
   end

   if type(minval) ~= "number" then
      error("minimum value not found")
   elseif type(maxval) ~= "number" then
      error("maximum value not found")
   end

   if not qtifile then return end

   qtifile:write(
      '<resprocessing>\n',
      '  <outcomes>\n',
      '    <decvar defaultval="0" maxvalue="#1" minvalue="0.0" varname="SCORE" vartype="Integer"/>\n')
   qtifile:write( '  </outcomes>\n')
   qtifile:write( '  <respcondition continue="Yes">\n')
   qtifile:write( '    <conditionvar>\n')
   qtifile:write( '      <or>\n')

   qtifile:write(
      '        <varequal case="No" respident="FIN00">'..
	 '<![CDATA['..minval..'|'..maxval..']]>'..
	 '</varequal>\n')

   qtifile:write( '      </or>\n')
   qtifile:write( '    </conditionvar>\n')
   qtifile:write( '    <setvar action="Add" varname="SCORE">0</setvar>\n')
   qtifile:write( '  </respcondition>\n')
   qtifile:write( '</resprocessing>\n')

end


----------------------------------------------------------------------------
-- Document header and footer
----------------------------------------------------------------------------
function qtiheader(opt)

   -- check mandatory opt
   if type(opt.filename) ~= "string" then
      error("no QTI output file")
   elseif type(opt.title) ~= "string" then
      error("no title")
   end

   meta = {}
   if type(opt.startdate) == 'string' then
      meta.START_DATE = opt.startdate
      meta.CONSIDER_START_DATE = 'True'
   end

   qtifile = io.open(opt.filename,"w")

   qtifile:write('<questestinterop>\n')
   qtifile:write('  <assessment title="'..(opt.title)..'">\n')
   qtifile:write('    <qticomment></qticomment>\n')
   qtifile:write('    <duration></duration>\n')

   qti_write_metadata{ defaults = assessment_metadata_defaults }
   --qti_write_metadata(qti_complete_assessment_metadata{})
   --qti_write_metadata(qti_complete_assessment_metadata(meta))

   qtifile:write(
      '<assessmentcontrol feedbackswitch="Yes" hintswitch="Yes"'..
	 ' solutionswitch="Yes" view="All"/>')

   qtifile:write('<rubric view="All">',qti_mattext(''),'</rubric>')

   qtifile:write(
      '<presentation_material>',
      '<flow_mat class="Block">',
      qti_mattext(''),
      '</flow_mat>',
      '</presentation_material>')

   qtifile:write(
      '<assessfeedback ident="Feedback" title="Feedback" view="All">',
      '<flow_mat class="Block">',
      qti_mattext(''),
      '</flow_mat>',
      '</assessfeedback>\n')
end

function qtifooter()
   qtifile:write('  </assessment>\n')
   qtifile:write('</questestinterop>\n')

   qtifile:close()
end


----------------------------------------------------------------------------
-- Section header and footer
----------------------------------------------------------------------------

function qtisectionheader(opt)

   -- return if there is no QTI file open
   if not qtifile then return end

   -- check mandatory opt
   if type(opt.title) ~= "string" then
      error("no title")
   end

   qtifile:write('<section title="'..(opt.title)..'">')

   qti_write_metadata{
      SECTION_INFORMATION  = '',
      SECTION_OBJECTIVE    = '',
      SECTION_KEYWORD      = '',
      SECTION_RUBRIC       = '',
      ATTACHMENT           = '',
      QUESTIONS_ORDERING   = '1'}

   qtifile:write(
      '<presentation_material><flow_mat class="Block">',
      qti_mattext(''),
      '<material><matimage embedded="base64" imagtype="text/html" uri=""/></material>',
      '</flow_mat></presentation_material>')

   qtifile:write(
      '<selection_ordering sequence_type="Normal">',
      '  <selection>',
      '    <sourcebank_ref/>',
      '    <selection_number/>',
      '  </selection>',
      '  <order order_type="Sequential"/>',
      '</selection_ordering>\n')

end

function qtisectionfooter()
   -- return if there is no QTI file open
   if not qtifile then return end

   qtifile:write('</section>\n')
end

----------------------------------------------------------------------------
-- Numerical question
----------------------------------------------------------------------------

function qtinumquestion(opt)

   -- return if there is no QTI file open
   if not qtifile then return end

   -- check mandatory options
   if type(opt.text) ~= "string" then
      error("no question text")
   end

   if type(opt.percentband) == "number" then
      minval = (100. - opt.percentband)/100. * opt.result
      maxval = (100. + opt.percentband)/100. * opt.result
   end

   -- check ordering, e.g. for negative numbers
   if minval > maxval then
      tmp = minval
      minval = maxval
      maxval = tmp
   end

   if type(minval) ~= "number" then
      error("minimum value not found")
   elseif type(maxval) ~= "number" then
      error("maximum value not found")
   end

   qtype = 'Numeric Response'

   qtifile:write('<item title="'..(qtype)..'">')
   qtifile:write('<duration/>')

   qtifile:write('<itemmetadata>')
   qti_write_metadata{qmd_itemtype = qtype}

   qti_write_metadata{
      TEXT_FORMAT     = 'HTML',
      ITEM_OBJECTIVE  = '',
      ITEM_KEYWORD    = '',
      ITEM_RUBRIC     = '',
      ITEM_TAGS       = '',
      ATTACHMENT      = ''}
   qtifile:write('</itemmetadata>')

   qtifile:write('<rubric view="All">',qti_mattext(''),'</rubric>')

   qtifile:write(
      '<presentation label="FIN"><flow class="Block">',
      qti_mattext(opt.text),
      qti_mattext(''),
      '<response_str ident="FIN00" rcardinality="Ordered" rtiming="No">',
      '  <render_fin columns="5" fintype="String" prompt="Box" rows="1"/>',
      '</response_str>',
      '</flow></presentation>')


   qtifile:write('<resprocessing>')

   qtifile:write(
      '<outcomes>',
      '<decvar defaultval="0" maxvalue="'..(opt.points)..'" minvalue="0.0"',
      ' varname="SCORE" vartype="Integer"/>',
      '</outcomes>\n')

   qtifile:write(
      '<respcondition continue="Yes">',
      '<conditionvar><or>',

      '<varequal case="No" respident="FIN00">',
      '<![CDATA['..minval..'|'..maxval..']]>',
      '</varequal>\n',

      '</or></conditionvar>',
      '<setvar action="Add" varname="SCORE">0</setvar>',
      '</respcondition>\n')

   qtifile:write( '</resprocessing>\n')
   qtifile:write('</item>')
end

----------------------------------------------------------------------------
-- Multiple Choice Question
----------------------------------------------------------------------------

function qtimultichoice(opt)

   -- return if there is no QTI file open
   if not qtifile then return end

   -- check mandatory options
   if type(opt.text) ~= "string" then
      error("no question text")
   end

   qtype = 'Multiple choice'

   qtifile:write('<item title="'..(qtype)..'">')
   qtifile:write('<duration/>')

   qtifile:write('<itemmetadata>')
   qti_write_metadata{qmd_itemtype = qtype}

   qti_write_metadata{
      TEXT_FORMAT     = 'HTML',
      ITEM_OBJECTIVE  = '',
      ITEM_KEYWORD    = '',
      ITEM_RUBRIC     = '',
      ITEM_TAGS       = '',
      ATTACHMENT      = '',
      hasRationale    = 'false',
      PARTIAL_CREDIT  = 'FALSE',
      RANDOMIZE       = 'false'
   }
   qtifile:write('</itemmetadata>')

   qtifile:write('<rubric view="All">',qti_mattext(''),'</rubric>')

   qtifile:write(
      '<presentation label="FIN"><flow class="Block">',
      qti_mattext(opt.text),
      qti_mattext(''))

   qtifile:write(
      '<response_lid ident="MCSC" rcardinality="Single" rtiming="No">',
      '<render_choice shuffle="No">')

   for i,c in ipairs (opt.choices) do
      qtifile:write(
	 '<response_label ident="',c.label,
	 '" rarea="Ellipse" rrange="Exact" rshuffle="Yes">',
	 qti_mattext(c.text),
	 '<material>',
	 '<matimage embedded="base64" imagtype="text/html" uri=""/>',
	 '</material>',
	 '</response_label>')
   end

   qtifile:write('</render_choice></response_lid>')
   qtifile:write('</flow></presentation>')


   qtifile:write('<resprocessing>')

   qtifile:write(
      '<outcomes>',
      '<decvar defaultval="0" maxvalue="'..(opt.points)..'" minvalue="0.0"',
      ' varname="SCORE" vartype="Integer"/>',
      '</outcomes>\n')

   for i,c in ipairs (opt.choices) do
      qtifile:write(
        '<respcondition continue="No">',
        '  <conditionvar>',
        '    <varequal case="Yes" respident="MCSC">'..(c.label)..'</varequal>',
        '  </conditionvar>',
        '  <setvar action="Add" varname="SCORE">0.0</setvar>',
        '  <displayfeedback feedbacktype="Response" linkrefid="'..(c.correct)..'"/>',
        '  <displayfeedback feedbacktype="Response" linkrefid="AnswerFeedback"><![CDATA[]]></displayfeedback>',
        '</respcondition>')
   end

   qtifile:write( '</resprocessing>\n')
   qtifile:write('</item>')
   end

----------------------------------------------------------------------------
-- Survey Matrix
----------------------------------------------------------------------------

function qtisurveymatrix(opt)

  -- return if there is no QTI file open
  if not qtifile then return end

  -- check mandatory options
  if type(opt.title) ~= "string" then
    error("no question text")
  end

  qtype = 'Survey Matrix'

  qtifile:write('<item title="'..(qtype)..'">')
  qtifile:write('<duration/>')

  qtifile:write('<itemmetadata>')
  qti_write_metadata{ qmd_itemtype = qtype }

  qti_write_metadata{
    TEXT_FORMAT                      = 'HTML',
    PREDEFINED_SCALE                 = 'EXCELLENT',
    ITEM_OBJECTIVE                   = '',
    ITEM_KEYWORD                     = '',
    ITEM_RUBRIC                      = '',
    ITEM_TAGS                        = '',
    ATTACHMENT                       = '',
    PREDEFINED_SCALE                 = 'EXCELLENT',
    FORCE_RANKING                    = 'false',
    ADD_COMMENT_MATRIX               = 'false',
    MX_SURVEY_QUESTION_COMMENTFIELD  = '200 character limit',
    MX_SURVEY_RELATIVE_WIDTH         = '0'
  }
  qtifile:write('</itemmetadata>')

  qtifile:write('<rubric view="All">',qti_mattext(''),'</rubric>')
  -- qtifile:write('<rubric view="All"/>')

  qtifile:write('<presentation><flow class="Block">')
  qtifile:write(qti_mattext(opt.title))
  qtifile:write(qti_matimage(''))


  qtifile:write(
    '<response_grp ident="resp_grp" rcardinality="Ordered" rtiming="No">',
    '<render_choice shuffle="No">')

  local nquestions
  for i,c in ipairs (opt.questions) do
    nquestions = i
    qtifile:write(
      '<response_label ident="MT-',opt.label,"-",i,
      '" rarea="Ellipse" rrange="Exact" rshuffle="Yes">',
      qti_mattext(c),
      '</response_label>')
  end

  for i,c in ipairs (opt.scores) do
     qtifile:write(
       '<response_label ident="MS-',opt.label,"-",i+nquestions,
       '" match_group="" match_max="',nquestions,
       '" rarea="Ellipse" rrange="Exact" rshuffle="Yes">',
       qti_mattext(c),
       '</response_label>')
  end

  qtifile:write('<response_label rarea="Ellipse" rrange="Exact" rshuffle="Yes"/>')

  qtifile:write('</render_choice></response_grp>')
  qtifile:write('</flow></presentation>')


  qtifile:write('<resprocessing>')

  qtifile:write(
    '<outcomes>',
    '<decvar defaultval="0" maxvalue="0" minvalue="0"',
    ' varname="SCORE" vartype="Integer"/>',
    '</outcomes>\n')

  qtifile:write( '</resprocessing>\n')


  qtifile:write('<itemfeedback ident="Correct" view="All">')
  qtifile:write('<flow_mat class="Block">')
  qtifile:write(qti_mattext(''))
  qtifile:write(qti_matimage(''))
  qtifile:write('</flow_mat>')
  qtifile:write('</itemfeedback>')

  qtifile:write('<itemfeedback ident="InCorrect" view="All">')
  qtifile:write('<flow_mat class="Block">')
  qtifile:write(qti_mattext(''))
  qtifile:write(qti_matimage(''))
  qtifile:write('</flow_mat>')
  qtifile:write('</itemfeedback>')


   -- for i,c in ipairs (opt.choices) do
   --    qtifile:write(
   --      '<respcondition continue="No">',
   --      '  <conditionvar>',
   --      '    <varequal case="Yes" respident="MCSC"></varequal>',
   --      '  </conditionvar>',
   --      '  <setvar action="Add" varname="SCORE">0.0</setvar>',
   --      '  <displayfeedback feedbacktype="Response" linkrefid="'..(c.correct)..'"/>',
   --      '  <displayfeedback feedbacktype="Response" linkrefid="AnswerFeedback"><![CDATA[]]></displayfeedback>',
   --      '</respcondition>')
   -- end

   qtifile:write('</item>')
   end



-- qti_write_metadata writes a metadata structure, described by its
-- arguments.  If a key 'defaults' is found in meta, it is used to
-- determine the order and the default values for all fields.
function qti_write_metadata(meta)

   -- return if there is no QTI file open
   if not qtifile then return end

   qtifile:write("<qtimetadata>")

   if type(meta.defaults) == "table" then
      for i,v in pairs(meta.defaults) do

        local key = v.name
        local value = v.default
        if type(meta[v.name]) == 'string' then
          --print ('found STRING for', v.name, ': ', meta[v.name])
          value = meta[v.name]
        end

        --print (key..' --> '..value..' ['..v.default..']')

        qtifile:write(
        ' <qtimetadatafield>',
        '   <fieldlabel>'..key..'</fieldlabel>',
        '   <fieldentry>'..value..'</fieldentry>',
        ' </qtimetadatafield>')
      end
    else
      for k,v in pairs(meta) do
        qtifile:write(' <qtimetadatafield>')
        qtifile:write('   <fieldlabel>'..k..'</fieldlabel>')
        qtifile:write('   <fieldentry>'..v..'</fieldentry>')
        qtifile:write(' </qtimetadatafield>')
      end
   end

   qtifile:write("</qtimetadata>")

end

function qti_mattext(txt)

   if txt=='' then

      return '<material>\n'..
	 '<mattext charset="ascii-us" texttype="text/plain" xml:space="default"/>\n'..
	 '</material>'

   else

      return '<material>'..
	 '<mattext charset="ascii-us" texttype="text/plain" xml:space="default">'..
	 '<![CDATA['..txt..']]>'..
	 '</mattext>'..
	 '</material>'

   end

end

function qti_matimage(url)

  return '<material>\n'..
  '<matimage embedded="base64" imagtype="text/html" uri="'..url..'"/>\n'..
  '</material>'

end

assessment_metadata_defaults = {
  { name='AUTHORS',					    default=''},
  { name='CREATOR',					    default=''},
  { name='SHOW_CREATOR',				    default='True'},
  { name='SCALENAME',					    default='STRONGLY_AGREE'},
  { name='EDIT_AUTHORS',				    default='True'},
  { name='EDIT_DESCRIPTION',				    default='True'},
  { name='ATTACHMENT',					    default=''},
  { name='DISPLAY_TEMPLATE',				    default='True'},
  { name='START_DATE',					    default=''},
  { name='END_DATE',					    default=''},
  { name='RETRACT_DATE',				    default=''},
  { name='CONSIDER_START_DATE',				    default='False'},
  { name='CONSIDER_END_DATE',				    default='False'},
  { name='CONSIDER_RETRACT_DATE',			    default='False'},
  { name='EDIT_END_DATE',				    default='True'},
  { name='EDIT_RETRACT_DATE',				    default='True'},
  { name='ASSESSMENT_RELEASED_TO',			    default='PHY1032S,2018'},
  { name='EDIT_PUBLISH_ANONYMOUS',			    default='True'},
  { name='EDIT_AUTHENTICATED_USERS',			    default='True'},
  { name='ALLOW_IP',					    default=''},
  { name='CONSIDER_ALLOW_IP',				    default='False'},
  { name='CONSIDER_USERID',				    default='False'},
  { name='PASSWORD',					    default=''},
  { name='EDIT_ALLOW_IP',				    default='True'},
  { name='EDIT_USERID',					    default='True'},
  { name='REQUIRE_LOCKED_BROWSER',			    default='False'},
  { name='EXIT_PASSWARD',				    default=''},
  { name='CONSIDER_DURATION',				    default='False'},
  { name='AUTO_SUBMIT',					    default='False'},
  { name='EDIT_DURATION',				    default='True'},
  { name='EDIT_AUTO_SUBMIT',				    default='True'},
  { name='NAVIGATION',					    default='RANDOM'},
  { name='QUESTION_LAYOUT',				    default='S'},
  { name='QUESTION_NUMBERING',				    default='CONTINUOUS'},
  { name='EDIT_NAVIGATION',				    default='True'},
  { name='EDIT_QUESTION_LAYOUT',			    default='True'},
  { name='EDIT_QUESTION_NUMBERING',			    default='True'},
  { name='MARK_FOR_REVIEW',				    default='False'},
  { name='LATE_HANDLING',				    default='True'},
  { name='MAX_ATTEMPTS',				    default='9999'},
  { name='EDIT_LATE_HANDLING',				    default='True'},
  { name='EDIT_MAX_ATTEMPTS',				    default='True'},
  { name='AUTO_SAVE',					    default='False'},
  { name='EDIT_AUTO_SAVE',				    default='True'},
  { name='EDIT_ASSESSFEEDBACK',				    default='True'},
  { name='SUBMISSION_MESSAGE',				    default='<![CDATA[]]>'},
  { name='FINISH_URL',					    default=''},
  { name='EDIT_FINISH_URL',				    default='True'},
  { name='FEEDBACK_DELIVERY',				    default='ON_SUBMISSION'},
  { name='FEEDBACK_COMPONENT_OPTION',			    default='SELECT_COMPONENTS'},
  { name='FEEDBACK_AUTHORING',				    default='BOTH'},
  { name='FEEDBACK_DELIVERY_DATE',			    default=''},
  { name='EDIT_FEEDBACK_DELIVERY',			    default='True'},
  { name='EDIT_FEEDBACK_COMPONENTS',			    default='True'},
  { name='FEEDBACK_SHOW_CORRECT_RESPONSE',		    default='False'},
  { name='FEEDBACK_SHOW_STUDENT_SCORE',			    default='True'},
  { name='FEEDBACK_SHOW_STUDENT_QUESTIONSCORE',		    default='True'},
  { name='FEEDBACK_SHOW_ITEM_LEVEL',			    default='True'},
  { name='FEEDBACK_SHOW_SELECTION_LEVEL',		    default='True'},
  { name='FEEDBACK_SHOW_GRADER_COMMENT',		    default='True'},
  { name='FEEDBACK_SHOW_STATS',				    default='True'},
  { name='FEEDBACK_SHOW_QUESTION',			    default='True'},
  { name='FEEDBACK_SHOW_RESPONSE',			    default='True'},
  { name='ANONYMOUS_GRADING',				    default='False'},
  { name='GRADE_SCORE',					    default='HIGHEST_SCORE'},
  { name='GRADEBOOK_OPTIONS',				    default='NONE'},
  { name='EDIT_GRADEBOOK_OPTIONS',			    default='True'},
  { name='EDIT_ANONYMOUS_GRADING',			    default='True'},
  { name='EDIT_GRADE_SCORE',				    default='True'},
  { name='BGCOLOR',					    default=''},
  { name='BGIMG',					    default=''},
  { name='EDIT_BGCOLOR',				    default='True'},
  { name='EDIT_BGIMG',					    default='True'},
  { name='EDIT_ASSESSMENT_METADATA',			    default='True'},
  { name='EDIT_COLLECT_SECTION_METADATA',		    default='True'},
  { name='EDIT_COLLECT_ITEM_METADATA',			    default='True'},
  { name='ASSESSMENT_KEYWORDS',				    default=''},
  { name='ASSESSMENT_OBJECTIVES',			    default=''},
  { name='ASSESSMENT_RUBRICS',				    default=''},
  { name='COLLECT_SECTION_METADATA',			    default='False'},
  { name='COLLECT_ITEM_METADATA',			    default=''},
  { name='LAST_MODIFIED_ON',				    default=''},
  { name='LAST_MODIFIED_BY',				    default=''},
  { name='templateInfo_isInstructorEditable',		    default='true'},
  { name='assessmentAuthor_isInstructorEditable',	    default='true'},
  { name='assessmentCreator_isInstructorEditable',	    default=''},
  { name='description_isInstructorEditable',		    default='true'},
  { name='dueDate_isInstructorEditable',		    default='true'},
  { name='retractDate_isInstructorEditable',		    default='true'},
  { name='anonymousRelease_isInstructorEditable',	    default='true'},
  { name='authenticatedRelease_isInstructorEditable',	    default='true'},
  { name='ipAccessType_isInstructorEditable',		    default='true'},
  { name='passwordRequired_isInstructorEditable',	    default='true'},
  { name='lockedBrowser_isInstructorEditable',		    default='true'},
  { name='timedAssessment_isInstructorEditable',	    default='true'},
  { name='timedAssessmentAutoSubmit_isInstructorEditable',  default='true'},
  { name='itemAccessType_isInstructorEditable',		    default='true'},
  { name='displayChunking_isInstructorEditable',	    default='true'},
  { name='displayNumbering_isInstructorEditable',	    default='true'},
  { name='displayScores_isInstructorEditable',		    default='true'},
  { name='submissionModel_isInstructorEditable',	    default='true'},
  { name='lateHandling_isInstructorEditable',		    default='true'},
  { name='instructorNotification_isInstructorEditable',	    default='True'},
  { name='automaticSubmission_isInstructorEditable',	    default='true'},
  { name='autoSave_isInstructorEditable',		    default=''},
  { name='submissionMessage_isInstructorEditable',	    default='true'},
  { name='finalPageURL_isInstructorEditable',		    default='true'},
  { name='feedbackType_isInstructorEditable',		    default='true'},
  { name='feedbackComponents_isInstructorEditable',	    default='true'},
  { name='testeeIdentity_isInstructorEditable',		    default='true'},
  { name='toGradebook_isInstructorEditable',		    default='true'},
  { name='recordedScore_isInstructorEditable',		    default='true'},
  { name='bgColor_isInstructorEditable',		    default='true'},
  { name='bgImage_isInstructorEditable',		    default='true'},
  { name='metadataAssess_isInstructorEditable',		    default='true'},
  { name='metadataParts_isInstructorEditable',		    default=''},
  { name='metadataQuestions_isInstructorEditable',	    default='true'},
  { name='honorpledge_isInstructorEditable',		    default='true'}
}

function qti_complete_assessment_metadata(input_metadata)

   default_metadata = {
     AUTHORS                  = '',
     CREATOR                  = '',
     SHOW_CREATOR             = 'True',
     SCALENAME                = 'STRONGLY_AGREE',
     EDIT_AUTHORS             = 'True',
     EDIT_DESCRIPTION         = 'True',
     ATTACHMENT               = '',
     DISPLAY_TEMPLATE         = 'True',

     START_DATE               = '',
     END_DATE                 = '',
     RETRACT_DATE             = '',
     CONSIDER_START_DATE      = 'False',
     CONSIDER_END_DATE        = 'False',
     CONSIDER_RETRACT_DATE    = 'False',
     EDIT_END_DATE            = 'True',
     EDIT_RETRACT_DATE        = 'True',

     ASSESSMENT_RELEASED_TO   = 'PHY1032S,2018',
     EDIT_PUBLISH_ANONYMOUS   = 'True',
     EDIT_AUTHENTICATED_USERS = 'True',

     ALLOW_IP = '',
     CONSIDER_ALLOW_IP= 'False',
     CONSIDER_USERID= 'False',
     PASSWORD= '',
     EDIT_ALLOW_IP= 'True',
     EDIT_USERID= 'True',
     REQUIRE_LOCKED_BROWSER= 'False',
     EXIT_PASSWARD= '',
     CONSIDER_DURATION= 'False',
     AUTO_SUBMIT= 'False',
     EDIT_DURATION= 'True',
     EDIT_AUTO_SUBMIT= 'True',
     NAVIGATION= 'RANDOM',
     QUESTION_LAYOUT= 'S',
     QUESTION_NUMBERING= 'CONTINUOUS',
     EDIT_NAVIGATION= 'True',
     EDIT_QUESTION_LAYOUT= 'True',
     EDIT_QUESTION_NUMBERING= 'True',
     MARK_FOR_REVIEW= 'False',
     LATE_HANDLING= 'True',
     MAX_ATTEMPTS= '9999',
     EDIT_LATE_HANDLING= 'True',
     EDIT_MAX_ATTEMPTS= 'True',
     AUTO_SAVE= 'False',
     EDIT_AUTO_SAVE= 'True',
     EDIT_ASSESSFEEDBACK= 'True',
     SUBMISSION_MESSAGE= '<![CDATA[]]>',
     FINISH_URL= '',
     EDIT_FINISH_URL= 'True',
     FEEDBACK_DELIVERY= 'ON_SUBMISSION',
     FEEDBACK_COMPONENT_OPTION= 'SELECT_COMPONENTS',
     FEEDBACK_AUTHORING= 'BOTH',
     FEEDBACK_DELIVERY_DATE= '',
     EDIT_FEEDBACK_DELIVERY= 'True',
     EDIT_FEEDBACK_COMPONENTS= 'True',
     FEEDBACK_SHOW_CORRECT_RESPONSE= 'False',
     FEEDBACK_SHOW_STUDENT_SCORE= 'True',
     FEEDBACK_SHOW_STUDENT_QUESTIONSCORE= 'True',
     FEEDBACK_SHOW_ITEM_LEVEL= 'True',
     FEEDBACK_SHOW_SELECTION_LEVEL= 'True',
     FEEDBACK_SHOW_GRADER_COMMENT= 'True',
     FEEDBACK_SHOW_STATS= 'True',
     FEEDBACK_SHOW_QUESTION= 'True',
     FEEDBACK_SHOW_RESPONSE= 'True',
     ANONYMOUS_GRADING= 'False',
     GRADE_SCORE= 'HIGHEST_SCORE',
     GRADEBOOK_OPTIONS= 'NONE',
     EDIT_GRADEBOOK_OPTIONS= 'True',
     EDIT_ANONYMOUS_GRADING= 'True',
     EDIT_GRADE_SCORE= 'True',
     BGCOLOR= '',
     BGIMG= '',
     EDIT_BGCOLOR= 'True',
     EDIT_BGIMG= 'True',
     EDIT_ASSESSMENT_METADATA= 'True',
     EDIT_COLLECT_SECTION_METADATA= 'True',
     EDIT_COLLECT_ITEM_METADATA= 'True',
     ASSESSMENT_KEYWORDS= '',
     ASSESSMENT_OBJECTIVES= '',
     ASSESSMENT_RUBRICS= '',
     COLLECT_SECTION_METADATA= 'False',
     COLLECT_ITEM_METADATA= '',
     LAST_MODIFIED_ON= '',
     LAST_MODIFIED_BY= '',
     templateInfo_isInstructorEditable= 'true',
     assessmentAuthor_isInstructorEditable= 'true',
     assessmentCreator_isInstructorEditable= '',
     description_isInstructorEditable= 'true',
     dueDate_isInstructorEditable= 'true',
     retractDate_isInstructorEditable= 'true',
     anonymousRelease_isInstructorEditable= 'true',
     authenticatedRelease_isInstructorEditable= 'true',
     ipAccessType_isInstructorEditable= 'true',
     passwordRequired_isInstructorEditable= 'true',
     lockedBrowser_isInstructorEditable= 'true',
     timedAssessment_isInstructorEditable= 'true',
     timedAssessmentAutoSubmit_isInstructorEditable = 'true',
     itemAccessType_isInstructorEditable = 'true',
     displayChunking_isInstructorEditable = 'true',
     displayNumbering_isInstructorEditable = 'true',
     displayScores_isInstructorEditable = 'true',
     submissionModel_isInstructorEditable = 'true',
     lateHandling_isInstructorEditable = 'true',
     instructorNotification_isInstructorEditable = 'True',
     automaticSubmission_isInstructorEditable = 'true',
     autoSave_isInstructorEditable = '',
     submissionMessage_isInstructorEditable = 'true',
     finalPageURL_isInstructorEditable = 'true',
     feedbackType_isInstructorEditable = 'true',
     feedbackComponents_isInstructorEditable = 'true',
     testeeIdentity_isInstructorEditable = 'true',
     toGradebook_isInstructorEditable = 'true',
     recordedScore_isInstructorEditable = 'true',
     bgColor_isInstructorEditable = 'true',
     bgImage_isInstructorEditable = 'true',
     metadataAssess_isInstructorEditable = 'true',
     metadataParts_isInstructorEditable = '',
     metadataQuestions_isInstructorEditable = 'true',
     honorpledge_isInstructorEditable = 'true',
   }

   for k in pairs(default_metadata)
   do
      if input_metadata[k] == nil
      then
	 input_metadata[k] = default_metadata[k]
      end
   end

   return input_metadata
end
