#!/usr/bin/env lua

-- This is a minimal demonstration how to create a QTI file from
-- Lua. It is a possible alternative to generating the file from
-- LuaLaTeX if a standalone QTI file, without accompanying
-- PDF/solutions/etc is required. The full power of Lua is still
-- available.

package.path = package.path .. ";../util/?.lua"

require 'qti'
require 'date'


qtiheader{
   filename="test.qti",
   title="An example Test",
}

qtisectionheader{title="Question 1"}

qtisectionfooter

qtifooter()
