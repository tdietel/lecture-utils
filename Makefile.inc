
UTILDIR := $(dir $(lastword $(MAKEFILE_LIST)))

# --------------------------------------------------------------------------
# set some defaults
TARGETS ?= $(wildcard slides*.tex notes*.tex eqnsheet*.tex examples*.tex handout*.tex)
TESTS ?= $(wildcard wps*.tex tut*.tex ct*.tex exam.tex defer*.tex supp*.tex)

TARGETS := $(patsubst %.tex,%,$(TARGETS))
TESTS := $(patsubst %.tex,%,$(TESTS))

LATEXMK = latexmk -use-make -lualatex -interaction=nonstopmode --shell-escape


.PHONY: all
#all: $(patsubst, %, %.pdf, $(TARGETS) $(TESTS))

all: $(patsubst %, %.pdf, $(TARGETS))
all: $(patsubst %, %.pdf, $(TESTS))
all: $(patsubst %, %-solution.pdf, $(TESTS))

showtargets:
	@echo $(patsubst %, %.pdf, $(TARGETS))
	@echo $(patsubst %, %.pdf, $(TESTS))
	@echo $(patsubst %, %-solution.pdf, $(TESTS))

# review: exam-review.pdf

# --------------------------------------------------------------------------
# sort tests by date

GETDATE=perl -ne 'print "$$1" if /header.*( \d+ [A-Z][a-z]* \d{4})/'

list:
	@for i in $(TESTS); do \
		/bin/echo -n "$$i  " ; \
		date -jf "%e %B %Y" "$$($(GETDATE) $$i.tex)" +"%s %F"; \
	done | sort -nk 2


# --------------------------------------------------------------------------
# rules for standard latex file:

%.pdf: %.tex FORCE
	$(LATEXMK) $(LATEX_ARGS) $<

%.:  %.tex ; $(LATEXMK) -pvc $(LATEX_ARGS) $<
%:   %.tex ; $(LATEXMK) -pvc $(LATEX_ARGS) $<

%-solution.pdf: %.tex
	$(LATEXMK) $(LATEX_ARGS) --jobname=$*-solution $<

%s: %.tex
	$(LATEXMK) -pvc $(LATEX_ARGS) --jobname=$*-solution $<



%-review.pdf: %.pdf %-solution.pdf
	echo $(shell echo "hons$$(date +%Y)cp")
	pdftk $^ output $@ user_pw $(shell echo "hons$$(date +%Y)cp")

FORCE: ;


# --------------------------------------------------------------------------
# collect old exam and class test papers

Exam_%.pdf: ../%/exam.pdf ; cp $< $@
Exam_%_Solution.pdf: ../%/exam-solution.pdf ; cp $< $@

ClassTest_%.pdf: ../%/ct.pdf ; cp $< $@
ClassTest_%_Solution.pdf: ../%/ct-solution.pdf ; cp $< $@


# --------------------------------------------------------------------------
# helpers: cleaning up, help text

.PHONY: clean
clean::
	rm -f *~
	rm -f *.aux *.log *.fls *.fdb_latexmk
	rm -f *.toc *.nav *.out *.snm *.vrb *.sta
	rm -f *.gnuplot *.table

.PHONY: reallyclean
reallyclean:: clean
	rm -f $(patsubst %, %.pdf, $(TARGETS) $(TESTS))
	rm -f $(patsubst %, %-solution.pdf, $(TESTS))

.PHONY: help
help: ## Print this help text
	@perl -nle 'printf("  %-20s %s\n",$$1,$$2) if /^(\S+):.*##\s*(.*)/' \
	$(MAKEFILE_LIST) | sort
