#!/usr/bin/env python3

import fileinput
import collections
import re


class re_pattern_collection (collections.OrderedDict):
    """Associate callables with regular expressions and execute them

    Regular expressions ("patterns") and callables ("actions") are added to
    this collections. The patterns are compiled into regular expressions.

    When the collection is applied to text, all patterns are matched to the
    input text in order.  If a match is found, the associated action is
    executed. If the  action returns None, the processing stops. Otherwise the
    text is updated with the return value and the processing continues with the
    next pattern."""


    def add(self,pat,action):

        if action is None:
            action = lambda m: None

        if not callable(action):
            raise TypeError("action must be callable")

        self[re.compile(pat)] = action

    def apply(self,text):

        # print(text)
        for p in self:
            m = p.match(text)
            # print ("TRY:", p, text)

            if m:
                action = patterns[p]
                text = action(m)
                # print ("  MATCH! new text:", text)
                if text is None:
                    return

            # print("FOO  ", p, " -> ", text)



# ----------------------------------------------------------------
# Some simple actions to be used
# ----------------------------------------------------------------

printline = lambda m: print(m.string,end='')

def bla(m):
    print("BLA:", m.string, m.groups() )
    return m.groups()[0]

# ----------------------------------------------------------------
# Define what should be done to the lines
# ----------------------------------------------------------------

patterns = re_pattern_collection()

# remove 2 leading spaces
patterns.add('  (.*)', lambda m: m.groups()[0]+'\n' )

# this is a dirty hack to add path to includegraphics
patterns.add('(.*){pde/(.*)',
    lambda m: m.expand('\\1{\\\\slidedir/pde/\\2\\n') )


# remove hspace and vspace - probably does not make sense in the notes
patterns.add('\\s*\\\\vspace{\d+[a-z]+}\\s*', None)
patterns.add('(.*)\\\\[hv]space{[0-9\.]+[a-z]+}(.*)',
    lambda m: m.groups()[0]+m.groups()[0]+'\n' )

# Remove beamer frames and convert slide titles to sections
patterns.add('\\\\begin{frame}.*', None)
patterns.add('\\\\end{frame}.*', None)

patterns.add('\\s*\\\\frametitle({.*}).*',
    lambda m: print("\\subsection{}".format(m.groups()[0])) )

# remove beamer columns
patterns.add('\s*\\\\begin{columns}.*', None)
patterns.add('\s*\\\\end{columns}.*', None)
patterns.add('\\s*\\\\column.*', None)

patterns.add('\\s*\\\\onslide[<0-9\->]*', None)


#
patterns.add('\s*\\\\\[\s*$', lambda m: '\\begin{equation}\n')
patterns.add('\s*\{\s*\\\\large\s*\\\\\[\s*', lambda m: '\\begin{equation}\n')
patterns.add('\s*\\\\\]\s*', lambda m: '\\end{equation}\n')

# by default, print the line
patterns.add('.*', printline)


# ----------------------------------------------------------------
# And apply the patterns to all input lines
# ----------------------------------------------------------------

print("\\documentclass[notes.tex]{subfiles}\n\n\\begin{document}\n")
for line in fileinput.input():
    patterns.apply(line)

print("\n\\end{document}\n")

    # print(line, end='')

    # print ("LINE:", line)
