#!/usr/bin/env python3

import re
import glob
from subprocess import check_output
import yaml
import argparse
from pprint import pprint

# ======================================================================
# find all available questions
# ======================================================================

def find_questions(questionfiles,trimstring):

    qdata = dict()

    for i in questionfiles:

        #m=re.search("%s/(\w+)/(\w+).tex"%testlibdir, i.decode("utf-8"))
        m=re.search("%s/(.*)/([a-zA-Z0-9-_]+).tex"%trimstring, i.decode("utf-8"))

        if (m):
            s = m.group(1)
            q = m.group(2)

            if not s in qdata:
                qdata[s] = dict()

            if not q in qdata[s]:
                qdata[s][q] = dict( [
                    ('section',s),
                    ('question',q),
                    ('filename',i.decode("utf-8")),
                    ('usage',dict())
                ] )

    return qdata


# ======================================================================
# fill usage information
# ======================================================================

def fill_usage_info(files, qdata):

    tdata = dict()

    for filename in files:

        m=re.match( ".*/(\d+)/(\w+).tex", filename.decode('utf-8'))
        if not m: continue

        y = m.group(1)
        t = m.group(2)

        questions = []

        # print ("y: %d   t: %s" % (int(y), t))

        with open(filename) as f:
            for lineno,line in enumerate(f):

                m=re.match( "^\s*.input\{.testlib/(.+)/([a-zA-Z0-9_\-]+).tex\}", line)
                if not m:
                    continue
                # print (m)
                s = m.group(1)
                q = m.group(2)

                # print ("y: %d   t: %s   s: %s   q: %s" % (int(y), t,s,q))

                if not s in qdata:
                    print( "unknown section %s found in file %s:%d:"
                           % (s,str(filename),lineno+1))
                    print(" ",line)

                if not q in qdata[s]:
                    print("unknown question in %s[%d]: %s/%s" % (t,y,s,q))

                qdata[s][q]['usage'][y] = t
                questions.append(dict( [ ('section', s), ('question', q) ] ))

        if questions != []:
            if not y in tdata:
                tdata[y] = dict()

            if not t in tdata[y]:
                tdata[y][t] = questions

    return tdata,qdata



# ======================================================================
# generate usage table (for LaTeX)
# ======================================================================


# ----------------------------------------------------------------------
# generate table header
def gen_usage_table_header(years):

    out = '\\begin{longtable}{|l|l|%s}\n' % ('c|' * len(years))
    #out = '\\begin{tabular}{|l|l|%s}\n' % ('c|' * len(years))

    out += "\\hline\n"

    out += "&"
    for y in years:
        out += "& %s " % y
    out += '\\\\\n'

    out += "\\hline\n" * 2

    return out

# ----------------------------------------------------------------------
# generate table footer
def gen_usage_table_footer():

    return "\\end{longtable}"
    #out += "\\end{tabular}"


# ----------------------------------------------------------------------
# generate complete table header
def gen_usage_table(qdata, years):

    out = ""

    out += gen_usage_table_header(years)
    for s in qdata:

        for q in sorted(qdata[s]):
            #print (qdata[s][q])
            out += "\\verb|{section}| & \\verb|{question}| ".format(**qdata[s][q])

            for y in years:
                out += " & "
                if y in qdata[s][q]['usage']:
                    out += qdata[s][q]['usage'][y]

            out += '\\\\\n'
        out += "\\hline\n"

    out += gen_usage_table_footer()

    return out

def gen_question_catalog(qdata,years):

    out = ""

    for s in qdata:

        out += "\n\n\\section{Category: \\texttt{%s}}\n\n" % s
        out += "\\begin{questions}\n"
        for q in sorted(qdata[s]):
            out += "  \\input{{\\testlib/{section}/{question}}}\n".format(**qdata[s][q])
            out += "  \\begin{tabular}{ll}\n"
            out += "  \\hline\n"
            out += "  {{\\small file name:}} & "
            out += "  {{\\small\\verb|{section}/{question}|}} \\\\\n".format(**qdata[s][q])
            out += "  \\end{tabular}\n"
        out += "\\end{questions}\n"

        #for q in sorted(qdata[s]):
        #    print (qdata[s][q])

    return out


# ======================================================================
# main function
# ======================================================================

#years = [2013,2014,2015,2016,2018]



#parser = argparse.ArgumentParser(description='Create a question catalog.')
#parser.add_argument('files', metavar='y', type=str, nargs='+',
#                    help='file names to be inspected')
#parser.add_argument('--sum', dest='accumulate', action='store_const',
#                    const=sum, default=max,
#                    help='sum the integers (default: find the max)')
#
#args = parser.parse_args()
#print(args.accumulate(args.integers))
#
#years = glob.glob("../20??/")
#

#print(args)

# ----------------------------------------------------------------------
# find the input files

questionfiles = check_output("find ../testlib -name '*.tex' -not -name 'testbank*' -not -name chapter20.tex", shell=True).splitlines()
testfiles = check_output("find ../20* -name '*.tex' -not -name 'testbank*' -not -name chapter20.tex", shell=True).splitlines()

# ----------------------------------------------------------------------
# parse file data

qdata = find_questions(questionfiles, "../testlib")
tdata,qdata = fill_usage_info(testfiles, qdata)

years = tdata.keys()

# pprint(testfiles)
# ----------------------------------------------------------------------
# produce output files

with open('questions.yaml', "w") as f:
    yaml.dump(qdata, f)

with open('summary.tex', "w") as f:
    f.write( gen_usage_table(qdata,years) )

with open('questions.tex', "w") as f:
    f.write( gen_question_catalog(qdata,years) )
