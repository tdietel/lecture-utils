
Lecture Utils
=============

This repository contains a few scripts and goodies for LaTeX that I
use for my lectures at the University of Cape Town. The repository
would usually be loaded as a submodule of a lecture repository, or it
can sit in a separate directory that is shared between several lecture
repositories.

Some of the goodies you will find in this repository are:

 - a Makefile to automate a lot of stuff
 - a system, based on LuaLaTeX, to automate numerical calculations in
   the solutions for problem sets, tests etc.
 - a generator for QTI files that can be used as Vula quizzes
 - a script to compile a catalog of all questions in a library,
   including a table to summarize the usage in previous years
 - loading of all the LaTeX packages that I like to use in my lectures

To use it, clone the repository, include the **Makefile.inc** in your
Makefile, and include **commands.tex** in your LaTeX sources.

Feel free to use it, if you find it of any value.
